import { DelPostUseCase } from '../../src/usecases/del-post.usecase';

describe('Delete Post', () => {
  let useCase;

  beforeEach(() => {
    useCase = new DelPostUseCase();
  });
  it('del', async () => {
    const id = 1;

    const response = await useCase.execute(id);

    expect(response).toEqual(200);
  });
});
