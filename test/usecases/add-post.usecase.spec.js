import { AddPostUseCase } from '../../src/usecases/add-posts.usecase';

describe('Add Post', () => {
  let useCase;

  beforeEach(() => {
    useCase = new AddPostUseCase();
  });
  it('add', async () => {
    const id = 200;
    const title = 'prueba';
    const body = 'prueba';

    const response = await useCase.execute(id, title, body);

    expect(response).toEqual(201);
  });
});
