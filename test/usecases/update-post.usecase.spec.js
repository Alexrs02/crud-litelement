import { UpdatePostsUseCase } from '../../src/usecases/update-posts.usecase';

describe('Add Post', () => {
  let useCase;

  beforeEach(() => {
    useCase = new UpdatePostsUseCase();
  });
  it('add', async () => {
    const id = 1;
    const title = 'prueba';
    const body = 'prueba';

    const response = await useCase.execute(id, title, body);

    expect(response).toEqual(200);
  });
});
