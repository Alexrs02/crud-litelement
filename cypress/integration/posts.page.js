/// <reference types="Cypress" />

beforeEach(() => {
  cy.intercept('GET', 'https://jsonplaceholder.typicode.com/posts', { fixture: './../../fixtures/posts.json' });
});

it('user click on odd button to show only odd posts', () => {
  cy.visit('/posts');
  cy.get('genk-posts').shadow().find('#oddAction').click();
  cy.wait(0);
  cy.get('genk-posts').shadow().find('posts-ui').shadow().find('#post_2').should('not.exist');
});

it('user add a post', () => {
  cy.visit('/posts');
  cy.get('post-detail').shadow().find('#title').type('Prueba Post');
  cy.get('post-detail').shadow().find('#body').type('Prueba Post');
  cy.get('post-detail').shadow().find('#update-btn').click();
  cy.wait(0);
  cy.get('genk-posts').shadow().find('posts-ui').shadow().find('#post_101').should('exist');
});

it('user update a post', () => {
  cy.visit('/posts');
  cy.get('genk-posts').shadow().find('posts-ui').shadow().find('#post_1').click();
  cy.wait(0);
  cy.get('post-detail').shadow().find('#title').clear().type('Updated Post', { force: true });
  cy.get('post-detail').shadow().find('#body').clear().type('Updated Post', { force: true });
  cy.get('post-detail').shadow().find('#update-btn').click();
  cy.wait(0);
  cy.get('genk-posts').shadow().find('posts-ui').shadow().find('#post_1').should('have.text', '\n                    1 -- Updated Post\n                ');
});

it('user delete a post', () => {
  cy.visit('/posts');
  cy.get('genk-posts').shadow().find('posts-ui').shadow().find('#post_3').click();
  cy.wait(0);
  cy.get('post-detail').shadow().find('#delete-btn').click();
  cy.wait(0);
  cy.get('genk-posts').shadow().find('posts-ui').shadow().find('#post_3').should('not.exist');
});
