import { css, html, LitElement } from 'lit';
import { UpdatePostsUseCase } from '../usecases/update-posts.usecase';
import { AddPostUseCase } from '../usecases/add-posts.usecase';
import { DelPostUseCase } from '../usecases/del-post.usecase';
import { snapshot, subscribe } from 'valtio/vanilla';
import { state } from '../states/state';

export class PostsUI extends LitElement {
  static get properties () {
    return {
      posts: {
        type: Array,
        state: true
      }
    };
  }

  static get styles () {
    return css`
            li{
                cursor: pointer;
                list-style: none;
                margin: 1rem 0 1rem 0;
            }
            li:hover{
                text-decoration: underline;
            }  
        `;
  }

  connectedCallback () {
    super.connectedCallback();
    document.addEventListener('restartPost', this.restartPost);
  }

  restartPost () {
    this.requestUpdate();
  }

  constructor () {
    super();
    this.restartPost = this.restartPost.bind(this);
  }

  render () {
    return html`
            <ul id="posts">
            ${this.posts && this.posts.map((post) => html`
                <li class="post" id="post_${post.id}" @click="${() => {
                  state.id = post.id;
                  state.title = post.title;
                  state.body = post.body;
                  state.action = 'update';
                }}">
                    ${post.id} -- ${post.title}
                </li>
            `)}
            </ul>
        `;
  }
}

customElements.define('posts-ui', PostsUI);
