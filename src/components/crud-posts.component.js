import { LitElement, css, html } from 'lit';
import './posts.component';
import './post-detail.component';

export class CrudPosts extends LitElement {
  render () {
    return html`
            <div>
                <genk-posts ></genk-posts>
                <post-detail></post-detail>
            </div>

        `;
  }

  createRenderRoot () {
    return this;
  }
}

customElements.define('crud-posts', CrudPosts);
