import { css, html, LitElement, unsafeCSS } from 'lit';
import { AllPostsUseCase } from '../usecases/all-posts.usecase';
import { OddPostsUseCase } from '../usecases/odd-posts.usecase';
import { UpdatePostsUseCase } from '../usecases/update-posts.usecase';
import { AddPostUseCase } from '../usecases/add-posts.usecase';
import { DelPostUseCase } from '../usecases/del-post.usecase';
import { PostsUI } from './../ui/posts.ui';
import { state } from '../states/state';

export class PostsComponent extends LitElement {
  static get styles () {
    return css`
            .container-posts{
                background-color: #a39eea;
                width: 30%;
                border-radius: .5rem;
                box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;
                margin-left: 2rem;
            }
            .container-buttons{
                display: flex;
                justify-content: center;
                flex-direction: row;
                column-gap: 2rem;
                
            }
            h1{
                color: #242642;
                padding-top: 1rem;
                text-align: center;
            }
            button{
                background-color: #242642;
                color: #ead9ed;
                width: 4rem;
                height: 2rem;
                border-radius: .5rem;
                border: 0;
            }
            button:hover{
                cursor: pointer;
                transform: translateY(-1px);
            }
        `;
  }

  static get properties () {
    return {
      posts: {
        type: Array,
        state: true
      },
      title: {
        type: String,
        state: true
      }

    };
  }

  async connectedCallback () {
    super.connectedCallback();
    document.addEventListener('crudEvent', this.manageEvent);
    this.restartPost = new CustomEvent('restartPost');
    const allPostsUseCase = new AllPostsUseCase();
    this.posts = await allPostsUseCase.execute();
  }

  constructor () {
    super();
    this.manageEvent = this.manageEvent.bind(this);
  }

  async allOdds () {
    const oddPostsUseCase = new OddPostsUseCase();
    this.posts = await oddPostsUseCase.execute();
  }

  addPosts () {
    this.addPostEvent = new CustomEvent('addPostEvent');
    document.dispatchEvent(this.addPostEvent);
  }

  manageEvent () {
    const action = state.action;
    switch (action) {
      case 'update':
        this.updatePost(state.id, state.title, state.body);
        const updatePostsUseCase = new UpdatePostsUseCase();
        this.posts = updatePostsUseCase.update(state.id, state.title, state.body, this.posts);
        document.dispatchEvent(this.restartPost);
        console.log('editado');
        break;
      case 'delete':
        this.delPost(state.id);
        this.posts.splice(state.id - 1, 1);
        document.dispatchEvent(this.restartPost);
        console.log('borrado');
        break;
      case 'add':
        this.addPost(state.id, state.title, state.body);
        const lastId = this.posts[this.posts.length - 1].id;
        const obj = { userId: 1, id: lastId + 1, title: state.title, body: state.body };
        this.posts.push(obj);
        document.dispatchEvent(this.restartPost);
        console.log('añadido');
        break;
    }
  }

  async addPost (id, title, body) {
    const addPostUseCase = new AddPostUseCase();
    this.state = await addPostUseCase.execute(id, title, body);
    if (this.state === 201) {
      alert('Post añadido');
    }
  }

  async delPost (id) {
    const delPostUseCase = new DelPostUseCase();
    this.state = await delPostUseCase.execute(id);
    if (this.state === 200) {
      alert('Borrado correctamente');
    }
  }

  async updatePost (id, title, body) {
    const updatePostUseCase = new UpdatePostsUseCase();
    this.state = await updatePostUseCase.execute(id, title, body);
    if (this.state === 200) {
      alert('Modificado correctamente');
    }
  }

  render () {
    return html`
            <div class="container-posts">
                <h1>POST PAGE</h1>
                <div class="container-buttons">
                    <button @click="${this.allOdds}" id="oddAction">Odd</button>
                    <button @click="${this.addPosts}" id="addAction">Add</button>
                </div>
                <posts-ui id="" .posts="${this.posts}"></posts-ui>
            </div>
            
        `;
  }
}

customElements.define('genk-posts', PostsComponent);
