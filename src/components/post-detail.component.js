import { LitElement, css, html } from 'lit';
import { subscribe } from 'valtio/vanilla';
import { state } from '../states/state';

export class PostDetail extends LitElement {
  static get styles () {
    return css`
            .container-details{
                background-color: #a39eea;
                position: fixed;
                width: 50%;
                height: 25rem;
                left: 40%;
                top: 20%;
                border-radius: .5rem;
                box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;
               
            }
            .title{
                font-size: 1.2rem;
                width: 100%;
                display: flex;
                justify-content: flex-start;
            }
            .inputs{
                font-size: 1.5rem;
                width: 100%;
                display: flex;
                flex-direction: column;
                align-items: center;
                row-gap: 2rem;
            }
            .input{
                display: flex;
                flex-direction: row;
                column-gap: 2rem;
            }
            input{
                border-radius: .5rem;
                border: 0;
                width: 20rem;
            }
            .details{
                display: flex;
                align-items: center;
                flex-direction: column;
                padding: 2rem;
                height: 100%;
                row-gap: 2rem;
            }
            h1{
                color: #242642;
            }

            button{
                background-color: #242642;
                color: #ead9ed;
                width: 6rem;
                height: 2.5rem;
                border-radius: .5rem;
                border: 0;
            }
            button:hover{
                cursor: pointer;
                transform: translateY(-1px);
            }

            .buttons{
                display: flex;
                flex-direction: row;
                width 100%;
                column-gap: 4rem;
            }
            #delete-btn{
                display: none;
            }

        `;
  }

  connectedCallback () {
    super.connectedCallback();
    subscribe(state, () => {
      this.renderRoot.querySelector('#id').value = state.id;
      this.renderRoot.querySelector('#title').value = state.title;
      this.renderRoot.querySelector('#body').value = state.body;
      if (state.action === 'update') {
        this.renderRoot.querySelector('#delete-btn').style.display = 'block';
        this.action = 'update';
        this.requestUpdate();
      } else {
        this.renderRoot.querySelector('#delete-btn').style.display = 'none';
        this.action = 'add';
        this.requestUpdate();
      }
      this.render();
    });
    document.addEventListener('addPostEvent', this.resetAction);
  }

  constructor () {
    super();
    this.action = 'add';
    this.resetAction = this.resetAction.bind(this);
  }

  resetAction () {
    state.id = 0;
    state.title = '';
    state.body = '';
    state.action = 'add';
  }

  setState (action) {
    state.title = this.renderRoot.querySelector('#title').value;
    state.body = this.renderRoot.querySelector('#body').value;
    state.id = this.renderRoot.querySelector('#id').value;
    state.action = action;
  }

  deleteAction () {
    this.setState('delete');
    this.crudEvent = new CustomEvent('crudEvent');
    document.dispatchEvent(this.crudEvent);
    this.resetAction();
  }

  add_updateAction () {
    if (this.action === 'add') {
      this.setState('add');
      this.crudEvent = new CustomEvent('crudEvent');
      document.dispatchEvent(this.crudEvent);
      this.resetAction();
    } else {
      this.setState('update');
      this.crudEvent = new CustomEvent('crudEvent');
      document.dispatchEvent(this.crudEvent);
      this.resetAction();
    }
  }

  render () {
    return html`
            <div class="container-details">
                <div class="details">
                    <div class="title">
                        <h2>POST DETAILS</h2>
                    </div>
                    <div class="inputs">
                        <div class="input">
                            <label>Title: </label>
                            <input id="title" type="text" placeholder="Title Post"/>
                        </div>
                        <div class="input">
                            <label>Body: </label>
                            <input type="text" id="body" placeholder="Body Post"/>
                            <input type="hidden" id="id">
                        </div>
                    </div>
                    <div class="buttons">
                        <button id="cancel-btn" @click="${this.resetAction}">Cancel</button>
                        <button id="update-btn" @click="${this.add_updateAction}">${this.action}</button>
                        <button id="delete-btn" @click="${this.deleteAction}">Delete</button>
                    </div>
                </div>
            </div>
        `;
  }
}

customElements.define('post-detail', PostDetail);
