import { LitElement, css, html } from 'lit';
import logo from '../img/logo.png';
export class LayoutApp extends LitElement {
  static get styles () {
    return css`              
                nav{
                    top: 0;
                    position: sticky;
                    display: flex;
                    align-items: center;
                    flex-direction: row;
                    justify-content: flex-start;
                    width: 100%;
                    border-radius: .5rem;
                    height: 3rem;
                    background-color: #242642;
                }
                a{
                    margin-left: 3rem;
                    text-decoration: none;
                    color: #ead9ed;
                }

                img{
                    width: 8rem;
                    padding: 2rem;
                }
            `;
  }

  render () {
    return html`
            <img src="${logo}" alt="logo-web">
            <nav>
              <a href="/">Home</a>
              <a href="/posts">Posts</a>
            </nav>
        `;
  }
}

customElements.define('layout-app', LayoutApp);
