import { LitElement, html } from 'lit';
import './../components/crud-posts.component';
import '../components/layout.component';

export class PostsPage extends LitElement {
  render () {
    return html`
            <layout-app></layout-app>
            <crud-posts></crud-posts>
        `;
  }

  createRenderRoot () {
    return this;
  }
}

customElements.define('posts-page-genk', PostsPage);
