import { LitElement, html } from 'lit';
import '../components/layout.component';

export class HomePage extends HTMLElement {
  constructor () {
    super();
  }

  connectedCallback () {
    this.innerHTML = `
        <layout-app></layout-app>
        <h1>Vanilla</h1>
        <app-element hello="Hola Pablo"><p>¿Qué se va a mostrar?</p></app-element>
    `;
  }
}

customElements.define('home-page-genk', HomePage);
