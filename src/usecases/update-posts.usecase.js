import { PostsRepository } from '../repositories/posts.repository';

export class UpdatePostsUseCase {
  update (id, title, body, posts) {
    const arr = posts;
    arr[id - 1].title = title;
    arr[id - 1].body = body;

    return arr;
  }

  async execute (id, title, body) {
    const repository = new PostsRepository();
    const state = await repository.updatePost(id, title, body);
    return state;
  }
}
