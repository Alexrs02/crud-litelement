import { PostsRepository } from '../repositories/posts.repository';

export class DelPostUseCase {
  async execute (id) {
    const repository = new PostsRepository();
    const state = await repository.deletePost(id);
    return state;
  }
}
