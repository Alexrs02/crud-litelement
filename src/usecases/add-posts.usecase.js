import { PostsRepository } from '../repositories/posts.repository';

export class AddPostUseCase {
  async execute (id, title, body) {
    const repository = new PostsRepository();
    const state = await repository.postPost(id, title, body);
    return state;
  }
}
