import axios from 'axios';

export class PostsRepository {
  async getAllPosts () {
    return await (
      await axios.get('https://jsonplaceholder.typicode.com/posts')
    ).data;
  }

  async postPost (id, title, body) {
    const resp = await axios({
      method: 'POST',
      url: 'https://jsonplaceholder.typicode.com/posts',
      data: {
        userId: 999,
        id: id,
        title: title,
        body: body
      }
    });
    return resp.status;
  }

  async updatePost (id, title, body) {
    const resp = await axios({
      method: 'PUT',
      url: `https://jsonplaceholder.typicode.com/posts/${id}`,
      data: {
        id: id,
        title: title,
        body: body
      }
    });
    return resp.status;
  }

  async deletePost (id) {
    const resp = await axios({ method: 'DELETE', url: `https://jsonplaceholder.typicode.com/posts/${id}` });
    return resp.status;
  }
}
